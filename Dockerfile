FROM jupyter/minimal-notebook

LABEL maintainer="Joe Marshall <joe.marshall@multiplytechnology.com>"

# Taken from https://github.com/jupyter/docker-stacks/blob/master/minimal-notebook/Dockerfile
USER root
# get some dependencies for opencv-python
RUN apt-get update --yes && \
    apt-get install --yes --no-install-recommends \
    python3-opencv && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR ${HOME}

# Install python packages
COPY requirements.txt ./
RUN pip3 install -r requirements.txt

# Move directory into container
COPY . .

# Fix permissions on mounted directory
RUN chown -R jovyan work/

USER ${NB_UID}